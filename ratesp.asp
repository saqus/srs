<!--METADATA
TYPE="TypeLib"
NAME="Microsoft ActiveX Data Objects 2.6 Library"
UUID="{00000206-0000-0010-8000-00AA006D2EA4}"
VERSION="2.6"
-->
<%@LANGUAGE="VBSCRIPT" CODEPAGE="65001"%>
<!--#include file="security.asp" -->
<!--#include file="Connections/insitedb.asp" -->

<%
', adBit, adParamInput,1,0
set spFavorite = Server.CreateObject("ADODB.Command")
spFavorite.ActiveConnection = MM_insitedb_STRING
spFavorite.CommandText = "srs_SP_insert_Favorite"
spFavorite.Parameters.Append spFavorite.CreateParameter("@ItemID", adVarChar, adParamInput,255,Request.QueryString("ItemId"))
spFavorite.Parameters.Append spFavorite.CreateParameter("@UserName", adVarChar, adParamInput,255,strUser)
spFavorite.Parameters.Append spFavorite.CreateParameter("@Favorite", adInteger, adParamInput, 1, Request.QueryString("f"))
spFavorite.CommandType = 4
spFavorite.CommandTimeout = 0
spFavorite.Prepared = true
spFavorite.Execute()
%>

<%
'Cooment line to view page  
if Request.QueryString("Page") = "rate" then
    Session("myLastPage")= Request.ServerVariables("HTTP_REFERER") & "#" & Request.QueryString("ItemId") 	
    Response.Redirect("rate.asp?ItemID=" & Request.QueryString("ItemId")) 
else
	Response.Redirect(Request.ServerVariables("HTTP_REFERER")& "#" & Request.QueryString("ItemId")) 	
end if
%>

<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="shortcut icon" href="assets/images/favicon.ico" />
<link rel="stylesheet" type="text/css" href="assets/css/stylesheet.css" media="all" />
<link href='http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700' rel='stylesheet' type='text/css'/>
<link rel="shortcut icon" href="assets/images/favicon2.ico" />
<title>SSRS Application</title>
</head>

<body>
<div class="wrapper">

<!-- #include file ="header.asp" -->


<div class="content searchpading">
  <h3>Interim page for testing</h3>
   <form action="<%=(Request.ServerVariables("HTTP_REFERER"))& "#" & (Request.QueryString("ItemId")) %>" method="get">
   <div class="advancesearch"> 
   	
    	<p>Your changes have been recorded. </p>
    	<p>Call usp to add / update Favorite indicator in Favorite table by RID /  user</p>
    	<p><%=(Request.ServerVariables("HTTP_REFERER"))& "#" & (Request.QueryString("ItemId")) %></p>
   </div>

	<div class="buttons">
    <button type="submit" value="ok" class="searchbutton">ok</button>
	</div>
</form> 

  </div>
  <div class="clear"></div>
<div class="clear"></div>



</div>
<!-- #include file ="footer.asp" -->

</div>
</body>
</html>