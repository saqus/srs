<script language="JavaScript1.2" type="text/javascript" src="assets/js/rowcolor.js"></script>
<!--#include file="security.asp" -->
<!--#include file="Connections/insitedb.asp" -->
<%
Dim rsReportList
Dim rsReportList_cmd
Dim rsReportList_numRows
Dim SortBy
SortBy = Request.QueryString("SortBy")
'response.write("SortBy='" + SortBy + "'")

Set rsReportList_cmd = Server.CreateObject ("ADODB.Command")
rsReportList_cmd.ActiveConnection = MM_insitedb_STRING
rsReportList_cmd.CommandText = "SELECT dbo.srs_VW_Reports.ItemID, dbo.srs_VW_Reports.Path, dbo.srs_VW_Reports.Name, dbo.srs_VW_Reports.ReportID, dbo.srs_VW_Reports.ModifiedDate, dbo.srs_VW_Reports.Views, dbo.srs_VW_Reports.AverageRate, dbo.srs_VW_Reports.NumberRate, dbo.srs_VW_Reports.GoldStd, dbo.srs_VW_Reports.BestPractice, dbo.srs_VW_Reports.Linked, dbo.srs_TBL_Favorite.Favorite, dbo.srs_TBL_Favorite.UserName, dbo.srs_TBL_Favorite.Rate, dbo.srs_TBL_Favorite.Favorite, dbo.srs_TBL_Favorite.ReportComment FROM dbo.srs_VW_Reports INNER JOIN dbo.srs_TBL_Favorite ON dbo.srs_VW_Reports.ItemID = dbo.srs_TBL_Favorite.ItemID WHERE dbo.srs_TBL_Favorite.Favorite <>0 and dbo.srs_TBL_Favorite.Favorite is not null and dbo.srs_TBL_Favorite.UserName = '"& strUser & "' ORDER BY " & SortBy
rsReportList_cmd.Prepared = true
Set rsReportList = rsReportList_cmd.Execute
rsReportList_numRows = 0
%>
<%
Dim Repeat1__numRows
Dim Repeat1__index

Repeat1__numRows = -1
Repeat1__index = 0
rsReportList_numRows = rsReportList_numRows + Repeat1__numRows
%>

<div class="content">
    <div class="wd report lft">	
      <h3>My Favorites: Sorted by <%response.write(SortBy)%></br >
	  Sort Favorites By: &nbsp<a href="favorite.asp?SortBy=Name">Report Name</a>
          &nbsp&nbsp|&nbsp&nbsp<a href="favorite.asp?SortBy=Views+desc"">Popularity (Views)</a>
          &nbsp&nbsp|&nbsp&nbsp<a href="favorite.asp?SortBy=AverageRate+desc"">Average User Rating</a>
      </h3>	
	
      <table width="100%" border="1" id="tableData">

<!-- Dims a variable to count the records returned, increments below fields -->

<% 
Dim numReturnedReports
Dim strNumReports
numReturnedReports = 0
%>

<% 
While ((Repeat1__numRows <> 0) AND (NOT rsReportList.EOF)) 
%>
          
<!-- Start Search Result -->
	<tr>
        <td>
        <a href= "../SRS_Report_Images/<%=(rsReportList.Fields.Item("ReportID").Value)%>.jpg" target="_self"><img class = "rht" src= "../SRS_Report_Images/tn_<%=(rsReportList.Fields.Item("ReportID").Value)%>.jpg" width="120" height="80" alt = "Click to see a larger image" title = "Click to see a larger image"/></a><br />
	</td>
	<td> 
    <a name="<%=(rsReportList.Fields.Item("ItemID").Value)%>" id="<%=(rsReportList.Fields.Item("ItemID").Value)%>"></a>
    <%if (rsReportList.Fields.Item("Favorite").Value)=0 then%>
        <a href="ratesp.asp?ItemID=<%=(rsReportList.Fields.Item("ItemID").Value)%>&amp;f=1&amp;a=<%=(rsReportList.Fields.Item("ItemID").Value)%>"><img src="assets/images/star2.jpg" alt = "Click star to add to favorites" title = "Click star to add to favorites"/></a>
    <%else%>
    	<a href="ratesp.asp?ItemID=<%=(rsReportList.Fields.Item("ItemID").Value)%>&amp;f=0&amp;a=<%=(rsReportList.Fields.Item("ItemID").Value)%>"><img src="assets/images/stars.jpg" alt = "Click star to remove from favorites" title = "Click star to remove from favorites"/></a>
    <%end if%>   &nbsp;       

    	
    <%if (rsReportList.Fields.Item("GoldStd").Value)="Y" then%>
          <img src="assets/images/Gold_Bar_Icon.jpg" width="20" height="20" alt = "Gold Standard Report" title = "Gold Standard Report"/> 
    <%end if%>
  <%if (rsReportList.Fields.Item("BestPractice").Value)="Y" then%>
              <img src="assets/images/Best_Practice_Icon.jpg" width="20" height="20" alt = "Best Practices Report" title = "Best Practices Report"/>
    <%end if%>
    <%if (rsReportList.Fields.Item("Linked").Value)="Y" then%>
          <img src="assets/images/Link.jpg" width="20" height="20" alt = "Linked Report" title = "Linked Report"/> 
    <%end if%>
          <a href="<%response.write(reportURL & replace(replace(replace((rsReportList.Fields.Item("Path").Value), " ", "+"), "_", "%5f"), "/", "%2f"))%>" target="_self"><%=(rsReportList.Fields.Item("Name").Value)%> </a> 
               (<%=(rsReportList.Fields.Item("ReportID").Value)%>)</a><br /> 

<!-- Removed for listfavorite.asp only -->
<!--
	  Description: <%'=(rsReportList.Fields.Item("FullDescription").Value)%></a><br />       
          Category:         <%'=(rsReportList.Fields.Item("Category").Value)%><br />
          Area:             <%'=(rsReportList.Fields.Item("FunctionalArea").Value)%><br />
     	  Key Fields:       <%'=(rsReportList.Fields.Item("Content").Value)%><br />
-->

          Date Modified:    <%=(rsReportList.Fields.Item("ModifiedDate").Value)%>
    <%if (rsReportList.Fields.Item("NumberRate").Value) = 0 then%>
          &nbsp;&nbsp;Average User Rating: <a href="ratesp.asp?ItemID=<%=(rsReportList.Fields.Item("ItemID").Value)%>&page=rate&f=1">Be the first to rate me!</a>
    <%else%>
          <img src="assets/images/<%=(rsReportList.Fields.Item("AverageRate").Value)%>.jpg" width="65" height="15"/>
          &nbsp;&nbsp; 
              <!--
              Average User Rating: <%=FormatNumber(rsReportList.Fields.Item("AverageRate").Value,1)%> of 10 
              -->
          <strong><a href="ratesp.asp?ItemID=<%=(rsReportList.Fields.Item("ItemID").Value)%>&page=rate&f=1">Rate Report</a></strong>
          &nbsp;&nbsp;(<%=(rsReportList.Fields.Item("NumberRate").Value)%>)
    <%end if%>        
          &nbsp;&nbsp;<%=(rsReportList.Fields.Item("Views").Value)%> Views<br />
	  <a href="ratecomment.asp?ItemID=<%=(rsReportList.Fields.Item("ItemID").Value)%>">See User Comments</a> 
	</td>
    
<%
numReturnedReports = numReturnedReports + 1
%>
	</tr>          

<!-- End Search Result -->      

<% 
  Repeat1__index=Repeat1__index+1
  Repeat1__numRows=Repeat1__numRows-1
  rsReportList.MoveNext()
Wend
strNumReports = "Number of Favorites: " & numReturnedReports
response.write(strNumReports)
%>
      </table>
    </div>
  </div>
  <div class="clear"></div>
  
<%
rsReportList.Close()
Set rsReportList = Nothing
%>
<script
script type="text/javascript">
   colorTableRows("tableData", "#FDF5E6");
</script> 