/**
 * Used to color alternating table rows
 *
 * @author: Gerard Uffelman - SaQus IT Solutions (gerard@saqus.com)
 *
 ******************************************************************************/


/**
 * Clickable tooltip. (Opens on an onclick event).
 * 
 ******************************************************************************/

function colorTableRows(tableId, rowColor)
{
	var tableElement = document.getElementById(tableId);
	
	var tableRows = tableElement.getElementsByTagName('TR');
	
	for(var i = 0; i < tableRows.length; i++)
	{
		if((i % 2) == 0)
		{
			var tableCells = tableRows[i].getElementsByTagName('TD');
			if(tableCells.length > 0)
			{
				colorCells(tableCells, rowColor);	
			}

		}
	}
}

function colorCells(tableCells, rowColor)
{
	for(var i = 0; i < tableCells.length; i++)
	{
		tableCells[i].style.backgroundColor = rowColor;
	}
}