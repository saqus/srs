<% 
    scr = Request.ServerVariables("SCRIPT_NAME") 
    if instr(scr,"/")>0 then 
        scr = right(scr, len(scr) - instrRev(scr,"/")) 
    end if  
%>
   <h1 class="lft"><a href="index.asp"><img src="assets/images/logo.jpg" /></a></h1>
  <div class="nav-menu lft">
    <ul>
<%if scr="index.asp" then%>
      <li><a href="index.asp" class="active">Home</a></li>
<%else%>
      <li><a href="index.asp">Home</a></li>
<%end if%>     

<%if scr="favorite.asp" then%>
      <li><a href="favorite.asp?SortBy=Name" class="active">My Favorites</a></li>
<%else%>
      <li><a href="favorite.asp?SortBy=Name">My Favorites</a></li>
<%end if%>     

<%if scr="category.asp" then%>
      <li><a href="category.asp?CoreFunction=0&SortBy=Views+desc" class="active">Report Category</a></li>
<%else%>
      <li><a href="category.asp?CoreFunction=0&SortBy=Views+desc">Report Category</a></li>
<%end if%>     

<%if scr="advanced.asp" then%>
      <li><a href="advanced.asp" class="active">Advanced Search</a></li>
<%else%>
      <li><a href="advanced.asp">Advanced Search</a></li>
<%end if%>

<li><a href="http://reporting.si-ns.siigroup.biz/Reports/Pages/Folder.aspx">Go to SRS Reports</a></li>     

    </ul>
  </div>
  <div class="searchbg rht">
    <div class="search">
      <form method="get" action="report.asp">
      <input type="text" name="search" placeholder="Enter Search Text" />
      <input type="hidden" name="SortBy" value="Views desc" />   
      <input type="submit" id="searchsubmit" value="Search"/>
      </form>
    </div>
  </div>
<%
'dim serverName
'serverName = Request.ServerVariables("SERVER_NAME")
'response.write serverName
%>

  <div class="clear"></div>