<script language="JavaScript1.2" type="text/javascript" src="assets/js/rowcolor.js"></script>
<!--#include file="security.asp" -->
<!--#include file="Connections/insitedb.asp" -->
<%
Dim strWhere
Dim strArea
Dim strCategory
Dim strCoreFunction
Dim strProcess
Dim strHeader

'<!-- If it is the first launch of the page, then no records will be returned -->

If Request.QueryString("CoreFunction")="0" then
    strWhere = " AND CoreFunction='0'"
    strHeader = "No Category or Area Selected"
Else
'<!-- Create where condition for filtering -->

strCategory = "Category: All Categories"
if Request.QueryString("Category") <> "" then
	strWhere = " AND Category='" & Request.QueryString("Category") & "'"
	strCategory = "Category: " & Request.QueryString("Category")
end if

strArea = "Area: All Areas"
if Request.QueryString("Area") <> "" then
	strWhere = strWhere & " AND FunctionalArea='" & Request.QueryString("Area") & "'"
	strArea = "Area: " & Request.QueryString("Area") 
end if

if Request.QueryString("Category") = "" AND Request.QueryString("Area") = "" then
		strWhere = strWhere & " AND FunctionalArea='XXX'"
end if

'strCoreFunction = "Core Function: All Core Functions"
'if Request.QueryString("CoreFunction") <> "" then
'	strWhere = strWhere & " AND CoreFunction='" & Request.QueryString("CoreFunction") & "'"
'	strCoreFunction = "Core Function: " & Request.QueryString("CoreFunction")
'end if

'strProcess = "Process: All Processes"
'if Request.QueryString("Process") <> "" then
'	strWhere = strWhere & " AND Process='" & Request.QueryString("Process") & "'"
'	strProcess = "Process: " & Request.QueryString("Process")
'end if
strHeader = strCategory & ",   " & strArea '& ", " & strCoreFunction & ", " & strProcess
end if
%>
<%
Dim rsCategory
Dim rsCategory_cmd
Dim rsCategory_numRows

Set rsCategory_cmd = Server.CreateObject ("ADODB.Command")
rsCategory_cmd.ActiveConnection = MM_insitedb_STRING
rsCategory_cmd.CommandText = "SELECT * FROM dbo.srs_VW_ReportCategory order by Category" 
rsCategory_cmd.Prepared = true

Set rsCategory = rsCategory_cmd.Execute
rsCategory_numRows = 0
%>
<%
Dim rsArea
Dim rsArea_cmd
Dim rsArea_numRows

Set rsArea_cmd = Server.CreateObject ("ADODB.Command")
rsArea_cmd.ActiveConnection = MM_insitedb_STRING
rsArea_cmd.CommandText = "SELECT * FROM dbo.srs_VW_ReportArea order by FunctionalArea" 
rsArea_cmd.Prepared = true

Set rsArea = rsArea_cmd.Execute
rsArea_numRows = 0
%>
<%
Dim rsCoreFunction
Dim rsCoreFunction_cmd
Dim rsCoreFunction_numRows

Set rsCoreFunction_cmd = Server.CreateObject ("ADODB.Command")
rsCoreFunction_cmd.ActiveConnection = MM_insitedb_STRING
rsCoreFunction_cmd.CommandText = "SELECT Distinct CoreFunction FROM dbo.srs_VW_ReportCoreFunction order by CoreFunction" 
rsCoreFunction_cmd.Prepared = true

Set rsCoreFunction = rsCoreFunction_cmd.Execute
rsCoreFunction_numRows = 0
%>
<%
Dim rsProcess
Dim rsProcess_cmd
Dim rsProcess_numRows

Set rsProcess_cmd = Server.CreateObject ("ADODB.Command")
rsProcess_cmd.ActiveConnection = MM_insitedb_STRING
rsProcess_cmd.CommandText = "SELECT Distinct Process FROM dbo.srs_VW_ReportProcess order by Process" 
rsProcess_cmd.Prepared = true

Set rsProcess = rsProcess_cmd.Execute
rsProcess_numRows = 0
%>
<%
Dim rsReportList
Dim rsReportList_cmd
Dim rsReportList_numRows
Dim SortBy
SortBy = Request.QueryString("SortBy")
'response.write("SortBy='" + SortBy + "'")

Set rsReportList_cmd = Server.CreateObject ("ADODB.Command")
rsReportList_cmd.ActiveConnection = MM_insitedb_STRING
rsReportList_cmd.CommandText = "SELECT dbo.srs_VW_Reports.*,isnull((SELECT Favorite From dbo.srs_TBL_Favorite Where dbo.srs_VW_Reports.ItemID = dbo.srs_TBL_Favorite.ItemID Collate Latin1_General_BIN and dbo.srs_TBL_Favorite.UserName = '"& strUser & "'),0) As Favorite, (SELECT UserName From dbo.srs_TBL_Favorite Where [dbo].[srs_VW_Reports].[ItemID] = dbo.srs_TBL_Favorite.ItemID Collate Latin1_General_BIN and dbo.srs_TBL_Favorite.UserName = '"& strUser & "') As UserName FROM dbo.srs_VW_Reports Where ModifiedDate >= '01/01/1990'" & strWhere & " ORDER BY " & SortBy  
 	
rsReportList_cmd.Prepared = true

'Dim qryText
'qryText = "SELECT * FROM dbo.srs_VW_Reports Where ModifiedDate >= '01/01/1990'"  & strWhere  & " order by Views desc " 
'response.write(qryText)

Set rsReportList = rsReportList_cmd.Execute
rsReportList_numRows = 0
%>

<%
Dim Repeat1__numRows
Dim Repeat1__index

Repeat1__numRows = -1
Repeat1__index = 0
rsReportList_numRows = rsReportList_numRows + Repeat1__numRows
%>
<%
Dim Repeat2__numRows
Dim Repeat2__index

Repeat2__numRows = -1
Repeat2__index = 0
rsCategory_numRows = rsCategory_numRows + Repeat2__numRows
%>
<%
Dim Repeat3__numRows
Dim Repeat3__index

Repeat3__numRows = -1
Repeat3__index = 0
rsArea_numRows = rsArea_numRows + Repeat3__numRows
%>
<div class="content">
  <h3>Filter by Area / CategorySort : Sorted by <%response.write(SortBy)%><br />
    Sort By: &nbsp<a href="category.asp?category=<%=Request.QueryString("Category")%>&Area=<%=Request.QueryString("Area")%>&SortBy=Name">Report Name</a>
    &nbsp&nbsp|&nbsp&nbsp<a href="category.asp?category=<%=Request.QueryString("Category")%>&Area=<%=Request.QueryString("Area")%>&SortBy=Views+desc">Popularity (Views)</a>
    &nbsp&nbsp|&nbsp&nbsp<a href="category.asp?category=<%=Request.QueryString("Category")%>&Area=<%=Request.QueryString("Area")%>&SortBy=AverageRate+desc" target="_self">Average User Rating</a>
  </h3>


<div class="category lft">
    <form id="form1" name="form1" method="get" action="category.asp">
      <h4>Category</h4>
<br />
<ul>
	<li>
		<a href="category.asp?category=&Area=<%=Request.QueryString("Area")%>&SortBy=Views+desc">
        	ALL Categories</a>
    </li>
<% 
While ((Repeat2__numRows <> 0) AND (NOT rsCategory.EOF)) 
%>
        <li>
	        <a class="tooltip" href="category.asp?category=<%=(rsCategory.Fields.Item("Category").Value)%>&Area=<%=Request.QueryString("Area")%>&SortBy=Views+desc" 
		    tooltip="<%=(rsCategory.Fields.Item("tooltip").Value)%>">
        	<%=(rsCategory.Fields.Item("Category").Value)%></a>
         </li>
<% 
  Repeat2__index=Repeat2__index+1
  Repeat2__numRows=Repeat2__numRows-1
  rsCategory.MoveNext()
Wend
%>
      </ul>
      <br /><br />
      <h4>Area</h4>

      <ul>
      	<li>
			<a href="category.asp?category=<%=Request.QueryString("Category")%>&Area=&SortBy=Views+desc">
        	ALL AREAS</a>
	    </li>
<%While (NOT rsArea.EOF)%>
	  	<li>
			<a href="category.asp?category=<%=Request.QueryString("Category")%>&Area=<%=(rsArea.Fields.Item("FunctionalArea").Value)%>&SortBy=Views+desc">
        	<%=(rsArea.Fields.Item("FunctionalArea").Value)%></a>
        </li>
<%
	  	rsArea.MoveNext()
			Wend
			If (rsArea.CursorType > 0) Then
			  rsArea.MoveFirst
			Else
		  rsArea.Requery
		End If
%>
     </ul>
    </form>
    </div>
	<div class="wd reportcategory lft">	

<% 'SEARCH RESULTS%>
<!--#include file="Results.asp" -->

    </div>
  </div>
  <div class="clear"></div>
  
<%
rsReportList.Close()
Set rsReportList = Nothing
%>
<script
script type="text/javascript">
   colorTableRows("tableData", "#FDF5E6");
</script>
<%
rsCategory.Close()
Set rsCategory = Nothing
%>
<%
rsArea.Close()
Set rsArea = Nothing
%>
