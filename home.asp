﻿<div class="content">
    <div class="rht"><img src="assets/images/welcome-image.jpg" /></div>
    <div class="index-page">
      <h2>Welcome</h2>
      <p>Welcome to the SRS Reports Page. Here you can search the report catalog, create a list of favorites, and see user ratings. </p>
      <p>If you need assistance, please contact the helpdesk at: (518) 347-4124.</p>
		<br></br>
		<br></br>
		<p>
            
        There are two types of special reports that we encourage people to use:
        <br /><br />
		- <a href="bestpractice.asp" target="_self">Best Practice Reports</a>
            meet standard reporting requirements across companies and can be used by any location.  They are identified in search results by
            <img src="assets/images/Best_Practice_Icon.jpg" width="20" height="20" alt = "Best Practices Report" title = "Best Practices Report"/>. 
		<br /><br />
		- <a href="goldstandard.asp" target="_self">Gold Standard Reports</a>
            use a single, validated source of data to provide consistent results.  Data is stored and refreshed daily for faster performance.  They are identified in
            search results by 
            <img src="assets/images/Gold_Bar_Icon.jpg" width="20" height="20" alt = "Gold Standard Report" title = "Gold Standard Report"/>.
		</p>
  	</div>
</div>
<div class="clear"></div>