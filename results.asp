<table width="100%" border="1" id="tableData">
<% 
Dim numReturnedReports
Dim strNumReports
numReturnedReports = 0
%>
<% 
While ((Repeat1__numRows <> 0) AND (NOT rsReportList.EOF)) 
%>

<!-- Start Search Result -->

	<tr>
    <td>
        <a href= "../SRS_Report_Images/<%=(rsReportList.Fields.Item("ReportID").Value)%>.jpg" target="_self"><img class = "rht" src= "../SRS_Report_Images/tn_<%=(rsReportList.Fields.Item("ReportID").Value)%>.jpg" width="180" height="120" alt = "Click to see a larger image" title = "Click to see a larger image"/></a><br /><br />
	</td>
	<td> 
    <a name="<%=(rsReportList.Fields.Item("ItemID").Value)%>" id="<%=(rsReportList.Fields.Item("ItemID").Value)%>"></a>
    <%if (rsReportList.Fields.Item("Favorite").Value)=0 or isnull((rsReportList.Fields.Item("Favorite").Value)) then%>
        <a href="ratesp.asp?ItemID=<%=(rsReportList.Fields.Item("ItemID").Value)%>&amp;f=1&amp;a=<%=(rsReportList.Fields.Item("ItemID").Value)%>"><img src="assets/images/star2.jpg" alt = "Click star to add to favorites" title = "Click star to add to favorites"/></a>
    <%else%>
    	<a href="ratesp.asp?ItemID=<%=(rsReportList.Fields.Item("ItemID").Value)%>&amp;f=0&amp;a=<%=(rsReportList.Fields.Item("ItemID").Value)%>"><img src="assets/images/stars.jpg" alt = "Click star to remove from favorites" title = "Click star to remove from favorites"/></a>
    <%end if%>   &nbsp;       
             
    <%if (rsReportList.Fields.Item("GoldStd").Value)="Y" then%>
          <img src="assets/images/Gold_Bar_Icon.jpg" width="20" height="20" alt = "Gold Standard Report" title = "Gold Standard Report"/> 
    <%end if%>
	<%if (rsReportList.Fields.Item("BestPractice").Value)="Y" then%>
              <img src="assets/images/Best_Practice_Icon.jpg" width="20" height="20" alt = "Best Practices Report" title = "Best Practices Report"/>
    <%end if%>
    <%if (rsReportList.Fields.Item("Linked").Value)="Y" then%>
          <img src="assets/images/Link.jpg" width="20" height="20" alt = "Linked Report" title = "Linked Report"/> 
    <%end if%>
    <a href="<%response.write(reportURL & replace(replace(replace((rsReportList.Fields.Item("Path").Value), " ", "+"), "_", "%5f"), "/", "%2f"))%>" target="_self"><%=(rsReportList.Fields.Item("Name").Value)%></a>
          (<%=(rsReportList.Fields.Item("ReportID").Value)%>)</a><br />  
     	  Description: <%=(rsReportList.Fields.Item("FullDescription").Value)%></a><br />        
          Category: <%=(rsReportList.Fields.Item("Category").Value)%><br />
          Area: <%=(rsReportList.Fields.Item("FunctionalArea").Value)%><br />
          Content: <%=(rsReportList.Fields.Item("Content").Value)%><br />
          
          Date Modified: <%=(rsReportList.Fields.Item("ModifiedDate").Value)%>&nbsp;&nbsp;
    <%if (rsReportList.Fields.Item("NumberRate").Value) = 0 then%>
          Average User Rating: <a href="ratesp.asp?ItemID=<%=(rsReportList.Fields.Item("ItemID").Value)%>&page=rate&f=0">Be the first to rate me!</a>
    <%else%>
          <img src="assets/images/<%=(rsReportList.Fields.Item("AverageRate").Value)%>.jpg" width="65" height="15"/>   <strong>
          <a href="ratesp.asp?ItemID=<%=(rsReportList.Fields.Item("ItemID").Value)%>&page=rate&f=<%=(rsReportList.Fields.Item("Favorite").Value)%>">Rate Report</a></strong>&nbsp;&nbsp;
          (<%=(rsReportList.Fields.Item("NumberRate").Value)%>)  &nbsp;&nbsp;
    <%end if%>        
          <%=(rsReportList.Fields.Item("Views").Value)%> Views&nbsp;&nbsp;
	  <a href="ratecomment.asp?ItemID=<%=(rsReportList.Fields.Item("ItemID").Value)%>">See User Comments</a> 
	</td>

<%
numReturnedReports = numReturnedReports + 1
%> 
	</tr>          
<!-- End Search Result -->   
<% 
	Repeat1__index=Repeat1__index+1
	Repeat1__numRows=Repeat1__numRows-1
	rsReportList.MoveNext()
	Wend
	strNumReports = "Returned Reports: " & numReturnedReports
	response.write(strNumReports)
%>
</table>

