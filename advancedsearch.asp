<!--#include file="Connections/insitedb.asp" -->
<%
Dim rsCategory
Dim rsCategory_cmd
Dim rsCategory_numRows

Set rsCategory_cmd = Server.CreateObject ("ADODB.Command")
rsCategory_cmd.ActiveConnection = MM_insitedb_STRING
rsCategory_cmd.CommandText = "SELECT * FROM dbo.srs_VW_ReportCategory Order by Category" 
rsCategory_cmd.Prepared = true

Set rsCategory = rsCategory_cmd.Execute
rsCategory_numRows = 0
%>
<%
Dim rsArea
Dim rsArea_cmd
Dim rsArea_numRows

Set rsArea_cmd = Server.CreateObject ("ADODB.Command")
rsArea_cmd.ActiveConnection = MM_insitedb_STRING
rsArea_cmd.CommandText = "SELECT * FROM dbo.srs_VW_ReportArea Order by FunctionalArea" 
rsArea_cmd.Prepared = true

Set rsArea = rsArea_cmd.Execute
rsArea_numRows = 0
%>
<%
Dim rsCoreFunction
Dim rsCoreFunction_cmd
Dim rsCoreFunction_numRows

Set rsCoreFunction_cmd = Server.CreateObject ("ADODB.Command")
rsCoreFunction_cmd.ActiveConnection = MM_insitedb_STRING
rsCoreFunction_cmd.CommandText = "SELECT * FROM dbo.srs_VW_ReportCoreFunction Order by CoreFunction" 
rsCoreFunction_cmd.Prepared = true

Set rsCoreFunction = rsCoreFunction_cmd.Execute
rsCoreFunction_numRows = 0
%>
<%
Dim rsProcess
Dim rsProcess_cmd
Dim rsProcess_numRows

Set rsProcess_cmd = Server.CreateObject ("ADODB.Command")
rsProcess_cmd.ActiveConnection = MM_insitedb_STRING
rsProcess_cmd.CommandText = "SELECT * FROM dbo.srs_VW_ReportProcess Order by Process" 
rsProcess_cmd.Prepared = true

Set rsProcess = rsProcess_cmd.Execute
rsProcess_numRows = 0
%>
<div class="content searchpading">
  <h3>Advanced Search</h3>
   <form action="report.asp" method="get">
   <div class="advancesearch"> 
   	<table width="100%" border="1">
 			 
  			<tr>
			    <td>Category</td>
		    	<td>
	                <select name="Category" id="Category">
	                  <option value="0">(All Categories)</option>
					  <%While (NOT rsCategory.EOF)%>
                      <option value="<%=(rsCategory.Fields.Item("Category").Value)%>"><%=(rsCategory.Fields.Item("Category").Value)%></option>
                      <%
                      rsCategory.MoveNext()
                      Wend
                      If (rsCategory.CursorType > 0) Then
                          rsCategory.MoveFirst
                      Else
                          rsCategory.Requery
                      End If
                      %>
				  </select>
                    Type of report
                 </td>
			 </tr>

  			<tr>
			    <td>Area</td>
		    	<td> 
             	  <select name="Area" id="Area">
	                  <option value="0">(All Areas)</option>
					  <%While (NOT rsArea.EOF)%>
                      <option value="<%=(rsArea.Fields.Item("FunctionalArea").Value)%>"><%=(rsArea.Fields.Item("FunctionalArea").Value)%></option>
                      <%
                      rsArea.MoveNext()
                      Wend
                      If (rsArea.CursorType > 0) Then
                          rsArea.MoveFirst
                      Else
                          rsArea.Requery
                      End If
                      %>
				  </select>
                    Functional area
                </td>
			 </tr>

			<tr>
			    <td>Core Function</td>
		    	<td> 
             	  <select name="CoreFunction" id="CoreFunction">
	                  <option value="0">(All Core Functions)</option>
					  <%While (NOT rsCoreFunction.EOF)%>
                      <option value="<%=(rsCoreFunction.Fields.Item("CoreFunction").Value)%>"><%=(rsCoreFunction.Fields.Item("CoreFunction").Value)%></option>        
                       <%
                      rsCoreFunction.MoveNext()
                      Wend
                      If (rsCoreFunction.CursorType > 0) Then
                          rsCoreFunction.MoveFirst
                      Else
                          rsCoreFunction.Requery
                      End If
                      %>
				  </select>
                    Purpose of report
                </td>
			 </tr>

			<tr>
			    <td>Process</td>
		    	<td> 
             	  <select name="Process" id="Process">
	                  <option value="0">(All Processes)</option>
					  <%While (NOT rsProcess.EOF)%>
                      <option value="<%=(rsProcess.Fields.Item("Process").Value)%>"><%=(rsProcess.Fields.Item("Process").Value)%></option> 
                       <%
                      rsProcess.MoveNext()
                      Wend
                      If (rsProcess.CursorType > 0) Then
                          rsProcess.MoveFirst
                      Else
                          rsProcess.Requery
                      End If
                      %>
				  </select>
                    Data provided
                </td>
			 </tr>
			
			<tr>
			   <td>Special Reports</td>
			<td>
          	  <select name="Report Type" id="Report Type">
          	    <option value="0">(All Reports)</option>
          	    <option value="Best Practices">Best Practice</option>
          	    <option value="Gold Standard">Gold Standard</option>
       	      	  </select>
                Best Practice and Gold Standard Reports
		</td>
			 </tr>

			<tr>
			   <td>Sort Records By</td>
			<td>
          	  <select name="SortBy" id="SortBy">
          	    <option value="Views" selected="selected">Popularity (Views)</option>
          	    <option value="Name">Report Name</option>
          	    <option value="AverageRate">Average User Rating</option>
       	      	  </select>
                Sort by name, views, or user rating
		</td>
			 </tr>
  			<tr>
			    <td>Keyword 1</td>
		    	<td> <input type="text" name="K1" size="30" /> Enter key field, report ID, or report name</td>
			 </tr>
			  <tr>
				<td>Keyword 2</td>
			    <td> <input type="text" name="K2" /> Enter key field, report ID, or report name</td>
			  </tr>
			  <tr>
			    <td>Keyword 3</td>
			    <td> <input type="text" name="K3" /> Enter key field, report ID, or report name</td>
			  </tr>
	 </table>
	</div>

	<div class="buttons">
		<button type="reset" value="Reset" class="reset">Reset</button>  
		<button type="submit" value="Search" class="searchbutton">Submit</button>
	</div>

</form>  
  </div>
  <div class="clear"></div>
<div class="clear"></div>