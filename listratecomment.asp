<script language="JavaScript1.2" type="text/javascript" src="assets/js/rowcolor.js"></script>
<!--#include file="security.asp" -->
<!--#include file="Connections/insitedb.asp" -->
<%
Dim strItemID
strItemID = Request.QueryString("ItemID")
'response.write("Item ID='" + ItemID + "'")

Dim rsReportList
Dim rsReportList_cmd
Dim rsReportList_numRows

Set rsReportList_cmd = Server.CreateObject ("ADODB.Command")
rsReportList_cmd.ActiveConnection = MM_insitedb_STRING
rsReportList_cmd.CommandText = "SELECT dbo.srs_TBL_Favorite.*, (SELECT Distinct dbo.srs_VW_Reports.Name From dbo.srs_VW_Reports WHERE dbo.srs_VW_Reports.ItemID = dbo.srs_TBL_Favorite.ItemID) as Name, (SELECT Distinct dbo.srs_VW_Reports.ReportID From dbo.srs_VW_Reports WHERE dbo.srs_VW_Reports.ItemID = dbo.srs_TBL_Favorite.ItemID) as ReportID FROM dbo.srs_TBL_Favorite WHERE dbo.srs_TBL_Favorite.ItemID = '" & strItemID & "'" 
rsReportList_cmd.Prepared = true

Set rsReportList = rsReportList_cmd.Execute
rsReportList_numRows = 0
%>
<%
Dim Repeat1__numRows
Dim Repeat1__index

Repeat1__numRows = -1
Repeat1__index = 0
rsReportList_numRows = rsReportList_numRows + Repeat1__numRows
%>

<div class="content">
    <div class="wd report lft">	
      <h3>User Comments about this Report</h3>
	You can submit or change your comments from the ratings page
      <table width="100%" border="1" id="tableData">
<% 
Dim numReturnedComments
Dim strNumComments
numReturnedComments = 0
%>
<% 
While ((Repeat1__numRows <> 0) AND (NOT rsReportList.EOF)) 
%>
<!-- Start Search Result -->
<%if ((rsReportList.Fields.Item("ReportComment").Value) = "") or (isnull(rsReportList.Fields.Item("ReportComment").Value)) THEN%>
 <%else %>
	<tr>

	<td> 
  
       
	  Report Name: <%=(rsReportList.Fields.Item("Name").Value)%></a><br />
          Report ID: <%=(rsReportList.Fields.Item("ReportID").Value)%></a><br />
	  Users's Name: <%=(rsReportList.Fields.Item("UserName").Value)%></a><br />
          User's Rating: <%=(rsReportList.Fields.Item("Rate").Value)%></a><br />
          Report Comments:         <%=(rsReportList.Fields.Item("ReportComment").Value)%><br />    
 
	</td>

	</tr>          
<%
numReturnedComments = numReturnedComments + 1
END IF
%>

<!-- End Search Result -->      

<% 
  Repeat1__index=Repeat1__index+1
  Repeat1__numRows=Repeat1__numRows-1
  rsReportList.MoveNext()
Wend
strNumComments = "Number of Comments: " & numReturnedComments
response.write(strNumcomments)
%>
      </table>
    </div>
  </div>
  <div class="clear"></div>
  
<%
rsReportList.Close()
Set rsReportList = Nothing
%>
<script
script type="text/javascript">
   colorTableRows("tableData", "#FDF5E6");
</script> 