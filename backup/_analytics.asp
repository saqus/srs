<!--#include file="Connections/insitedb.asp" -->
<!--#include file="security.asp" -->

<%
	Dim varAD_UserName
	varAD_UserName = strUser  'from security.asp
		
	Dim varActionDateTime
	varActionDateTime = (now)
	
	Dim varUserAction
	varUserAction = Request.querystring("action")
	If Len(varUserAction & "") = 0  then
		varUserAction=""
	elseif varUserAction = "null" then
		varUserAction=""
	end if
	
	Dim varHostName
	varHostName = Request.ServerVariables("REMOTE_HOST")
	
	'only return page name
	Dim varWebPageName
	Dim varURL
	Dim varLeft
	Dim varLeftSplit
	varURL = Request.ServerVariables("HTTP_REFERER")
	QLoc = InStr(varURL, "?")
	if QLoc = 0 then
		varLeft = varURL
	else
		varLeft = Left(varURL, QLoc-1)
	end if
	QSlash = InStrRev(varLeft, "/")
	varWebPageName = Mid(varLeft, QSlash+1)
	
	Dim varSearchText
	varSearchText = Request.querystring("search")
	If Len(varSearchText & "") = 0  then
		varSearchText=""
	elseif varSearchText = "null" then
		varSearchText=""
	end if
	
	Dim varResultsCount
	varResultsCount = Request.querystring("cnt")
	if IsNull(varResultsCount) then
		varResultsCount=0
	elseif varResultsCount = "null" then
		varResultsCount=0
	end if
	
    Dim sqlInsert
	
    Set sqlInsert = Server.CreateObject ("ADODB.Command")
    sqlInsert.ActiveConnection = MM_insitedb_STRING
    sqlInsert.CommandText = "INSERT INTO dbo.srs_TBL_Analytics ( AD_UserName, ActionDateTime, UserAction, HostName, WebPageName, SearchText, ResultsCount  ) VALUES (?,?,?,?,?,?,? ) " 
    sqlInsert.Prepared = true
    sqlInsert.Parameters.Append sqlInsert.CreateParameter("param1", 202, 1, 255, varAD_UserName) ' adDouble
    sqlInsert.Parameters.Append sqlInsert.CreateParameter("param2", 133, 1, 255, varActionDateTime) ' adDBDate
    sqlInsert.Parameters.Append sqlInsert.CreateParameter("param3", 200, 1, 255, varUserAction) ' adVarChar
	sqlInsert.Parameters.Append sqlInsert.CreateParameter("param4", 200, 1, 255, varHostName) ' adVarChar
	sqlInsert.Parameters.Append sqlInsert.CreateParameter("param5", 200, 1, 255, varWebPageName) ' adVarChar
	sqlInsert.Parameters.Append sqlInsert.CreateParameter("param6", 200, 1, 255, varSearchText) ' adVarChar
	sqlInsert.Parameters.Append sqlInsert.CreateParameter("param7", 3, 1, 255, varResultsCount) ' adInteger
    sqlInsert.Execute
    sqlInsert.ActiveConnection.Close

%>
