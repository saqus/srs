<!--#include file="security.asp" -->
<!--#include file="Connections/insitedb.asp" -->
<%
	Dim myStr1
	Dim RExp
	
	myStr1 = Request.QueryString("ReportComment") 

	Set RExp = New RegExp
	RExp.Pattern = "[^a-z A-Z0-9&-&_]"
	If RExp.Test(myStr1) Then
		Response.Redirect("error.asp")	
	End If
%>
<%
Dim MM_editAction
MM_editAction = CStr(Request.ServerVariables("SCRIPT_NAME"))
If (Request.QueryString <> "") Then
  MM_editAction = MM_editAction & "?" & Server.HTMLEncode(Request.QueryString)
End If

' boolean to abort record edit
Dim MM_abortEdit
MM_abortEdit = false
%>
<%
' IIf implementation
Function MM_IIf(condition, ifTrue, ifFalse)
  If condition = "" Then
    MM_IIf = ifFalse
  Else
    MM_IIf = ifTrue
  End If
End Function
%>
<%
If (CStr(Request("MM_update")) = "form1") Then
  If (Not MM_abortEdit) Then
    ' execute the update
    Dim MM_editCmd

    Set MM_editCmd = Server.CreateObject ("ADODB.Command")
    MM_editCmd.ActiveConnection = MM_insitedb_STRING
    MM_editCmd.CommandText = "UPDATE dbo.srs_TBL_Favorite SET Rate = ?, ReportComment = ? WHERE ItemID = ? AND UserName = ? " 
    MM_editCmd.Prepared = true
    MM_editCmd.Parameters.Append MM_editCmd.CreateParameter("param1", 5, 1, -1, MM_IIF(Request.Form("select"), Request.Form("select"), null)) ' adDouble
    MM_editCmd.Parameters.Append MM_editCmd.CreateParameter("param2", 202, 1, 255, Request.Form("ReportComment")) ' adVarWChar
    MM_editCmd.Parameters.Append MM_editCmd.CreateParameter("param3", 200, 1, 255, Request.Form("MM_recordId")) ' adVarChar
    MM_editCmd.Parameters.Append MM_editCmd.CreateParameter("param4", 200, 1, 50, strUser) ' adVarChar
    MM_editCmd.Execute
    MM_editCmd.ActiveConnection.Close

    ' append the query string to the redirect URL
    Dim MM_editRedirectUrl
    MM_editRedirectUrl = "success.asp"
    If (Request.QueryString <> "") Then
      If (InStr(1, MM_editRedirectUrl, "?", vbTextCompare) = 0) Then
        MM_editRedirectUrl = MM_editRedirectUrl & "?" & Request.QueryString
      Else
        MM_editRedirectUrl = MM_editRedirectUrl & "&" & Request.QueryString
      End If
    End If
    Response.Redirect(MM_editRedirectUrl)
  End If
End If
%>
<%
Dim rsData__MMColParam1
rsData__MMColParam1 = "1"
If (Request.QueryString("ItemID") <> "") Then 
  rsData__MMColParam1 = Request.QueryString("ItemID")
End If
%>
<%
Dim rsData__MMColParam2
rsData__MMColParam2 = strUser
%>
<%
Dim rsData
Dim rsData_cmd
Dim rsData_numRows

Set rsData_cmd = Server.CreateObject ("ADODB.Command")
rsData_cmd.ActiveConnection = MM_insitedb_STRING
rsData_cmd.CommandText = "SELECT * FROM dbo.srs_TBL_Favorite WHERE ItemID = ? AND UserName = ?" 
rsData_cmd.Prepared = true
rsData_cmd.Parameters.Append rsData_cmd.CreateParameter("param1", 200, 1, 255, rsData__MMColParam1) ' adVarChar
rsData_cmd.Parameters.Append rsData_cmd.CreateParameter("param2", 200, 1,  50, rsData__MMColParam2) ' adBoolean

Set rsData = rsData_cmd.Execute
rsData_numRows = 0
%>
<div class="content searchpading">
  <h3>Rate and Comment on Reports</h3>
   <form name="form1" action="<%=MM_editAction%>" method="POST">
   <div class="advancesearch"> 
   	<table width="100%" border="1">
  		<tr>
          <td>
            <h3>Report Comments <br />
		    (Up to 255 Characters)</h3>
		    <textarea name="ReportComment" rows="4" cols="50"><%=(rsData.Fields.Item("ReportComment").Value)%></textarea>
		  </td>
          <td>
      	    <h3>Rate Report <%=(rsData.Fields.Item("UserName").Value)%><br />
		    (From 1 to 10, 10 = highest rating)</h3>
             <%'Response.Write(Session("myLastPage"))%>
		  <p>
          	<select name="select" id="select">
          	<option value="1" <%If (Not isNull((rsData.Fields.Item("Rate").Value))) Then If ("1" = CStr((rsData.Fields.Item("Rate").Value))) Then Response.Write("selected=""selected""") : Response.Write("")%>>1</option>
          	<option value="2" <%If (Not isNull((rsData.Fields.Item("Rate").Value))) Then If ("2" = CStr((rsData.Fields.Item("Rate").Value))) Then Response.Write("selected=""selected""") : Response.Write("")%>>2</option>
          	<option value="3" <%If (Not isNull((rsData.Fields.Item("Rate").Value))) Then If ("3" = CStr((rsData.Fields.Item("Rate").Value))) Then Response.Write("selected=""selected""") : Response.Write("")%>>3</option>
          	<option value="4" <%If (Not isNull((rsData.Fields.Item("Rate").Value))) Then If ("4" = CStr((rsData.Fields.Item("Rate").Value))) Then Response.Write("selected=""selected""") : Response.Write("")%>>4</option>
          	<option value="5" <%If (Not isNull((rsData.Fields.Item("Rate").Value))) Then If ("5" = CStr((rsData.Fields.Item("Rate").Value))) Then Response.Write("selected=""selected""") : Response.Write("")%>>5</option>
          	<option value="6" <%If (Not isNull((rsData.Fields.Item("Rate").Value))) Then If ("6" = CStr((rsData.Fields.Item("Rate").Value))) Then Response.Write("selected=""selected""") : Response.Write("")%>>6</option>
          	<option value="7" <%If (Not isNull((rsData.Fields.Item("Rate").Value))) Then If ("7" = CStr((rsData.Fields.Item("Rate").Value))) Then Response.Write("selected=""selected""") : Response.Write("")%>>7</option>
          	<option value="8" <%If (Not isNull((rsData.Fields.Item("Rate").Value))) Then If ("8" = CStr((rsData.Fields.Item("Rate").Value))) Then Response.Write("selected=""selected""") : Response.Write("")%>>8</option>
          	<option value="9" <%If (Not isNull((rsData.Fields.Item("Rate").Value))) Then If ("9" = CStr((rsData.Fields.Item("Rate").Value))) Then Response.Write("selected=""selected""") : Response.Write("")%>>9</option>
          	<option value="10" <%If (Not isNull((rsData.Fields.Item("Rate").Value))) Then If ("10" = CStr((rsData.Fields.Item("Rate").Value))) Then Response.Write("selected=""selected""") : Response.Write("")%>>10</option>
       	    </select>
         </p>
		</td>
      </tr>
	 </table>
	</div>
	
	<div class="buttons">
		<button type="reset" value="Reset" class="reset">Reset</button>
		<button type="submit" value="Search" class="searchbutton">Submit</button>
	</div>

    <input type="hidden" name="MM_update" value="form1" />
    <input type="hidden" name="MM_recordId" value="<%= rsData.Fields.Item("ItemID").Value %>" />
  </form>  
  </div>
  <div class="clear"></div>
<div class="clear"></div>
<%
rsData.Close()
Set rsData = Nothing
%>
