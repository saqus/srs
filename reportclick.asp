<%@LANGUAGE="VBSCRIPT" CODEPAGE="65001"%>
<!--#include file="security.asp" -->
<!--#include file="Connections/insitedb.asp" -->
<%
Dim rsReportList
Dim rsReportList_cmd
Dim rsReportList_numRows

Set rsReportList_cmd = Server.CreateObject ("ADODB.Command")
rsReportList_cmd.ActiveConnection = MM_insitedb_STRING
rsReportList_cmd.CommandText = "SELECT * FROM dbo.srs_VW_Reports Where ReportID = '"  & ucase(Request.QueryString("ID"))  & "'"
rsReportList_cmd.Prepared = true

Set rsReportList = rsReportList_cmd.Execute
rsReportList_numRows = 0
%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Untitled Document</title>
</head>

<body>
<h1>Interim page for testing </h1>
<h3>Logic  </h3>
<p>1 - Log Report Click = add +1 to # of views for specified report</p>
<p>2 – Redirect user to report</p>
<p>Risk  - since the view count functionality is on the application side, unsuccessful attempts will be longer which could bloat the results.  For example, if a user clicks the link but does not have access to the report, it will still count the click. Another example is if the report does not work or run properly, the user may continue to click the link server times, which will increment the results.  </p>
<p>For better metrics, consider moving this logic to the SRS report generation side.</p>
<p>Report ID: <%=(rsReportList.Fields.Item("ReportID").Value)%></a><br />
</p>
</body>
</html>
