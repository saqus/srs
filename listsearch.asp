<script language="JavaScript1.2" type="text/javascript" src="assets/js/rowcolor.js"></script>
<!--#include file="security.asp" -->
<!--#include file="Connections/insitedb.asp" -->
<%
	Dim myStr1
	Dim myStr2
	Dim myStr3
	Dim myStr4
	Dim RExp
	
	myStr1 = Request.QueryString("search") 
	myStr2 = Request.QueryString("K1") 
	myStr3 = Request.QueryString("K2") 
	myStr4 = Request.QueryString("K3") 

	Set RExp = New RegExp
	RExp.Pattern = "[^a-z A-Z0-9&-&_&(&)]"
	If RExp.Test(myStr1) Then
		Response.Redirect("error.asp")
	elseIf RExp.Test(myStr2) Then
		Response.Redirect("error.asp")
	elseIf RExp.Test(myStr3) Then
		Response.Redirect("error.asp")
	elseIf RExp.Test(myStr4) Then
		Response.Redirect("error.asp")		
	End If
%>
<%
Dim strWhere
Dim strArea
Dim strCategory
Dim strReportType
Dim strCoreFunction
Dim strProcess
Dim strHeader

strWhere = ""

if Request.QueryString("Category") <> "" then
	strCategory = "Category: All Categories"
	if Request.QueryString("Category") <> "0" then
		strWhere = strWhere & " AND Category='" & Request.QueryString("Category") & "'"
		strCategory = "Category: " & Request.QueryString("Category")
	end if

	strArea = "Area: All Areas"
	if Request.QueryString("Area") <> "0" then
		strWhere = strWhere & " AND FunctionalArea='" & Request.QueryString("Area") & "'"
		strArea = "Area: " & Request.QueryString("Area") 
	end if

	strCoreFunction = "Core Function: All Core Functions"
	if Request.QueryString("CoreFunction") <> "0" then
		strWhere = strWhere & " AND CoreFunction='" & Request.QueryString("CoreFunction") & "'"
		strCoreFunction = "Core Function: " & Request.QueryString("CoreFunction")
	end if

	strProcess = "Process: All Processes"
	if Request.QueryString("Process") <> "0" then
		strWhere = strWhere & " AND Process ='" & Request.QueryString("Process") & "'"
		strProcess = "Process: " & Request.QueryString("Process")
	end if
	
	strReportType = "Report Type: All Report Types"
	if Request.QueryString("Report Type") <> "0" then
		if Request.QueryString("Report Type") = "Best Practices" then
			strReportType = "Report Type: Best Practices"
			strWhere = strWhere & " AND dbo.srs_VW_Reports.BestPractice = 'Y'"
		end if
		if Request.QueryString("Report Type") = "Gold Standard" then
			strReportType = "Report Type: Gold Standard"
			strWhere = strWhere & " AND dbo.srs_VW_Reports.GoldStd = 'Y'"
		end if
	end if
	
'	strSortBy = "Sort Reports By: Popularity (Views)"
'		if Request.QueryString("SortBy") = "Views" then
'			strReportType = "Sort By: Popularity (Views)"
'			strWhere = strWhere & " ORDER BY dbo.srs_VW_Reports.Views"
'		end if
'		if Request.QueryString("SortBy") = "Name" then
'			strReportType = "Sort By: Report Name"
'			strWhere = strWhere & " order by dbo.srs_VW_Reports.Name desc"
'		end if
'		if Request.QueryString("SortBy") = "AverageRate" then
'			strReportType = "Sort By: Average User Rating"
'			strWhere = strWhere & " order by dbo.srs_VW_Reports.AverageRate desc"
'		end if
	
end if
'response.write strWhere
if Request.QueryString("search") = "" then
	strHeader = strCategory & ", " & strArea & ", " & strCoreFunction & ", " & strProcess & ", " & strReportType '& ", " & strSortBy
else  
	strHeader = Request.QueryString("search") 
end if
%>
<%
Dim rsReportList
Dim rsReportList_cmd
Dim rsReportList_numRows
Dim strSearchCriteria
Dim SortBy

SortBy = Request.QueryString("SortBy")

'response.write("SortBy='" + SortBy + "'")

Set rsReportList_cmd = Server.CreateObject ("ADODB.Command")
rsReportList_cmd.ActiveConnection = MM_insitedb_STRING

if Request.QueryString("k1") = "" and Request.QueryString("k2") = "" and Request.QueryString("k3") = "" and strWhere = "" then 
	if Request.QueryString("search") = "" then
		Response.Redirect("error.asp")
	else
		rsReportList_cmd.CommandText = "SELECT dbo.srs_VW_Reports.*,isnull((SELECT Favorite From dbo.srs_TBL_Favorite Where dbo.srs_VW_Reports.ItemID = dbo.srs_TBL_Favorite.ItemID Collate Latin1_General_BIN and dbo.srs_TBL_Favorite.UserName = '"& strUser & "'), 0) As Favorite, (SELECT UserName From dbo.srs_TBL_Favorite Where [dbo].[srs_VW_Reports].[ItemID] = dbo.srs_TBL_Favorite.ItemID Collate Latin1_General_BIN and dbo.srs_TBL_Favorite.UserName = '"& strUser & "') As UserName FROM dbo.srs_VW_Reports Where SearchText Like '%" & ucase(Request.QueryString("search")) & "%'" & strWhere & " ORDER BY " & SortBy
		strSearchCriteria = Request.QueryString("search")
	end if
else
	rsReportList_cmd.CommandText = "SELECT dbo.srs_VW_Reports.*,isnull((SELECT Favorite From dbo.srs_TBL_Favorite Where dbo.srs_VW_Reports.ItemID = dbo.srs_TBL_Favorite.ItemID Collate Latin1_General_BIN and dbo.srs_TBL_Favorite.UserName = '"& strUser & "'), 0) As Favorite, (SELECT UserName From dbo.srs_TBL_Favorite Where [dbo].[srs_VW_Reports].[ItemID] = dbo.srs_TBL_Favorite.ItemID Collate Latin1_General_BIN and dbo.srs_TBL_Favorite.UserName = '"& strUser & "') As UserName FROM dbo.srs_VW_Reports Where SearchText Like '%" & ucase(Request.QueryString("k1")) & "%' AND SearchText Like '%" & ucase(Request.QueryString("k2")) & "%' AND SearchText Like '%" & ucase(Request.QueryString("k3")) & "%'" & strWhere & " ORDER BY " & SortBy
	strSearchCriteria = Request.QueryString("k1")& " " & Request.QueryString("k2") & " " & Request.QueryString("k3")
end if

rsReportList_cmd.Prepared = true

Set rsReportList = rsReportList_cmd.Execute
rsReportList_numRows = 0
%>
<%
Dim Repeat1__numRows
Dim Repeat1__index

Repeat1__numRows = -1
Repeat1__index = 0
rsReportList_numRows = rsReportList_numRows + Repeat1__numRows
%>

<div class="content">
    <div class="wd report lft">	
      <h3>Search Results for: <%=strHeader%><br />
     	<%if Request.QueryString("search") <> "" then%>
	Sort By: &nbsp<a href="report.asp?search=<%=Request.QueryString("search")%>&SortBy=Name">Report Name</a>
        &nbsp&nbsp|&nbsp&nbsp<a href="report.asp?search=<%=Request.QueryString("search")%>&SortBy=Views+desc">Popularity (Views)</a>
        &nbsp&nbsp|&nbsp&nbsp<a href="report.asp?search=<%=Request.QueryString("search")%>&SortBy=AverageRate+desc" >Average User Rating</a>
<%end if %>     
</h3>   
	

<% 'SEARCH RESULTS%>
<!--#include file="Results.asp" -->

    </div>
  </div>
  <div class="clear"></div>
  
<%
rsReportList.Close()
Set rsReportList = Nothing
%>
<script type="text/javascript">
   colorTableRows("tableData", "#FDF5E6");
</script>